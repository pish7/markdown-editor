import Note from '@/Note'
import Notes from '@/Notes'

let myNotes: Notes

describe('Notes 对象初始化', () => {
  test('初始化 [不提供参数]', ()=>{
    myNotes = new Notes()
    expect(myNotes.length).toBe(0)

    myNotes.add(new Note(0))
    expect(myNotes.length).toBe(1)
  })

  test('初始化 [使用数字和对象]', () => {
    myNotes = new Notes([
      {
        title: 'C',
        favorite: true,
        created: 1000,
        id: '1000',
      },
      2,
      3,
      {
        title: 'A',
        favorite: false,
        created: 1,
        id: '1',
      },
      {
        title: 'BB',
        favorite: true,
        created: 2,
        id: '2',
      },
    ])
  })

  test('信息检索', () => {
    expect(myNotes.length).toBe(5)
    const note = myNotes.getNoteFromId('2') as Note
    expect(note.created).toBe(2)
    myNotes.delete(note)
    expect(myNotes.length).toBe(4)

    const notes = myNotes.notes
    expect(notes[0].title).toBe('C')
    expect(notes[1].title).toBe('A')
    expect(notes[2].title).toBe('New note 3')
  })
})
