import Note from '@/Note'

const content =
  '**Hi!** This notebook is using [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) for formatting!'

describe('Note 对象初始化', () => {
  test('使用数字初始化', () => {
    const time = Date.now()
    const note = new Note(2)
    expect(note.favorite).toBe(false)
    expect(note.title).toBe('New note 3')
    expect(note.content).toBe(content)
    expect(typeof note.created).toBe('number')
    expect(time).toBeLessThanOrEqual(note.created)
    expect(note.created - time).toBeLessThan(1000)
    expect(String(note.created)).toEqual(note.id)
  })
  test('使用 Note 对象初始化', () => {
    const x = new Note(2)
    const note = new Note(x)
    expect(note.id).toBe(x.id)
    expect(note.content).toBe(x.content)
    expect(note.favorite).toBe(x.favorite)
    expect(note.created).toBe(x.created)
    expect(note.title).toBe(x.title)
  })
  test('使用一般对象初始化 [id为合法值]', () => {
    const x = {
      content: 'abc',
      favorite: 3,
      created: 100,
      id: '100',
    }
    const note = new Note(x)
    expect(note.id).toBe(x.id)
    expect(note.content).toBe(x.content)
    expect(note.favorite).toBe(true)
    expect(note.created).toBe(x.created)
    expect(note.title).toBe('New note')
  })
  test('使用一般对象初始化 [id为0]', () => {
    const time = Date.now()
    const x = {
      content: 'abc',
      favorite: 3,
      created: 0,
      id: '0',
    }
    const note = new Note(x)
    expect(note.content).toBe(x.content)
    expect(note.favorite).toBe(true)
    expect(note.title).toBe('New note')

    expect(typeof note.created).toBe('number')
    expect(time).toBeLessThanOrEqual(note.created)
    expect(note.created - time).toBeLessThan(1000)
    expect(String(note.created)).toEqual(note.id)
  })
  test('使用一般对象初始化 [id与created不匹配]', () => {
    const time = Date.now()
    const x = {
      content: 'abc',
      favorite: 3,
      created: 23,
      id: '0',
    }
    const note = new Note(x)
    expect(note.content).toBe(x.content)
    expect(note.favorite).toBe(true)
    expect(note.title).toBe('New note')

    expect(typeof note.created).toBe('number')
    expect(time).toBeLessThanOrEqual(note.created)
    expect(note.created - time).toBeLessThan(1000)
    expect(String(note.created)).toEqual(note.id)
  })
  test('使用空对象初始化', () => {
    const time = Date.now()
    const x = {}
    const note = new Note(x)

    expect(note.title).toBe('New note')
    expect(note.content).toBe(content)
    expect(note.favorite).toBe(false)

    expect(typeof note.created).toBe('number')
    expect(time).toBeLessThanOrEqual(note.created)
    expect(note.created - time).toBeLessThan(1000)
    expect(String(note.created)).toEqual(note.id)
  })
  test('使用非法类型初始化', () => {
    const x: any = true
    expect(() => {
      new Note(x)
    }).toThrow()
  })
})

describe('Note 转换', () => {
  test('Markdown 转 HTML', () => {
    const note = new Note(2)
    note.content = '## Hello'
    const html = note.preview
    expect(html.trim()).toBe('<h2 id="hello">Hello</h2>')
  })
})

describe('Note 统计', () => {
  test('行数', () => {
    const note = new Note(2)
    note.content = '## Hello\n### Hello'
    expect(note.linesCount).toBe(2)
  })

  test('字数', () => {
    const note = new Note(2)
    note.content = '## Hello\n### Hello'
    expect(note.wordsCount).toBe(4)
  })
})
