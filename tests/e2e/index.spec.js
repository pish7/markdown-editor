const { testWithSpectron } = require('vue-cli-plugin-electron-builder')

const markdownContent =
  '**Hi!** This notebook is using [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) for formatting!'
let stopServe, electron, client, win, contents
const count = 10

jest.setTimeout(100000)

beforeAll(async () => {
  ;({
    app: { electron, client, browserWindow: win, webContents: contents },
    stopServe,
  } = await testWithSpectron())
})

afterAll(async () => {
  // contents.executeJavaScript(`
  // localStorage.clear()
  // `)
  await stopServe()
})

// 正常启动窗口
describe('窗口启动', function() {
  test('启动一个窗口', async () => {
    expect(await client.getWindowCount()).toBe(1)
    expect(await win.isMinimized()).toBe(false)
    expect(await win.isVisible()).toBe(true)
    expect(await win.isNormal()).toBe(true)
    expect(await win.isMenuBarVisible()).toBe(false) // 关闭菜单栏
    const { width, height } = await win.getBounds()
    expect(width).toBeGreaterThan(0)
    expect(height).toBeGreaterThan(0)
  })

  test('开发者工具已关闭', async () => {
    const devToolsAreOpen = await contents.isDevToolsOpened()
    expect(devToolsAreOpen).toBe(false)
  })

  test('窗口标题', async () => {
    const title = await client.getTitle()
    expect(title).toBe('electron-markdown-vue-ts-sass')
  })
})

// 正常显示定义的界面元素
describe('显示的界面元素', function() {
  test('添加笔记按钮', async () => {
    const hello = await client.getText('.toolbar button')
    expect(hello).toBe('add 添加笔记')
  })

  test('删除所有笔记', async () => {
    const list = await client.$$('.note-list .note')
    console.log('删除前笔记数量:', list.length)
    for (let i = 0; i < list.length; i++) {
      await client.click('.note-list .note:nth-clild(1)')
      await client.click('.delete')
    }
  })

  test('列表为空', async () => {
    const list = await client.$$('.note-list .note')
    expect(list.length).toBe(0)
  })

  test('编辑区为空', async () => {
    const edit = await client.$('.edit')
    expect(edit.state).toBe('failure')
  })
})

describe('功能测试', function() {
  test('点击添加笔记按钮', async () => {
    for (let i = 0; i < count; i++) {
      await client.click('.note-list button')
    }
  })

  test('列表非空', async () => {
    const list = await client.$$('.note-list .note')
    expect(list.length).toBe(count)
  })

  test('列表标题正确', async () => {
    for (let i = 1; i <= count; i++) {
      const text = await client.getText(`.note-list .note:nth-child(${i})`)
      expect(text.trim()).toBe('New note ' + i)
    }
  })

  test('选中笔记前编辑区为空', async () => {
    const edit = await client.$('.edit')
    expect(edit.state).toBe('failure')
  })

  test('选中笔记后编辑区非空', async () => {
    await client.click(`.note-list .note:nth-child(1)`)
    const text = await client.getText('.edit')
    expect(text.length).toBeGreaterThan(0)

    const markdown = await client.getValue('.edit textarea')
    expect(markdown).toBe(markdownContent)
  })

  test('编辑区显示正常', async () => {
    await client.setValue('.edit textarea', '# Hello\n## World')
    expect(await client.getText('.edit .preview h1')).toBe('Hello')
    expect(await client.getText('.edit .preview h2')).toBe('World')
  })

  test('收藏笔记', async () => {
    await client.click('.edit button.favorite')
    const star = await client.getText('.note-list .note:nth-child(1) i')
    expect(star).toBe('star')
  })

  test('删除笔记', async () => {
    await client.click('.edit button.delete')
    await client.alertAccept() // 删除确认对话框点击“接受”
    const list = await client.$$('.note-list .note')
    expect(list.length).toBe(count - 1)
  })

  test('localStorage', async () => {
    await client.click(`.note-list .note:nth-child(1)`) // 先选中一篇笔记

    let notes = await client.localStorage('GET', 'notes')
    notes = JSON.parse(notes.value)
    expect(notes.length).toBe(count - 1)

    let note = notes[0]
    expect(typeof note.id).toBe('string')
    expect(typeof note.created).toBe('number')
    expect(String(note.created)).toBe(note.id)
    expect(typeof note.title).toBe('string')
    expect(typeof note.content).toBe('string')
    expect(typeof note.favorite).toBe('boolean')

    let selectedId = await client.localStorage('GET', 'selected-id')
    selectedId = JSON.parse(selectedId.value)
    expect(typeof selectedId).toBe('number')

    let index = notes.findIndex(val => val.id == String(selectedId))
    expect(index).toBeGreaterThanOrEqual(0)
  })
})
