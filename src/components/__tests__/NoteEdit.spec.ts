import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils'
import filter_date, { date } from '@/filters/date'
import Note from '@/Note'
import NoteEdit from '../NoteEdit.vue'

const content =
  '**Hi!** This notebook is using [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) for formatting!'

// 创建一个扩展的 `Vue` 构造函数
const localVue = createLocalVue()
localVue.use(filter_date)

describe('NoteEdit.vue', () => {
  describe('默认笔记内容', () => {
    const note = new Note(0)
    const wrapper = shallowMount(NoteEdit, {
      propsData: { note },
      localVue,
    })
    const el = wrapper.element

    test('输入框 [笔记标题]', () => {
      const input = el.querySelector('.toolbar input') as HTMLInputElement
      expect(input.value).toBe('New note 1')
    })

    test('按钮', () => {
      const buttons = wrapper.findAll('button').wrappers
      expect(buttons.length).toBe(2)
      expect(
        ['star_border', 'delete'].indexOf(buttons[0].text().trim())
      ).not.toBe(-1)
      expect(
        ['star_border', 'delete'].indexOf(buttons[1].text().trim())
      ).not.toBe(-1)

      expect(
        ['收藏', '删除'].indexOf(buttons[0].attributes('title') as string)
      ).not.toBe(-1)
      expect(
        ['收藏', '删除'].indexOf(buttons[1].attributes('title') as string)
      ).not.toBe(-1)
    })

    test('MarkDown 输入区', () => {
      const area = wrapper.find('textarea')
      const el = area.element as HTMLTextAreaElement
      expect(el.value.trim()).toBe(content)
    })

    test('MarkDown 输出区', () => {
      const preview = wrapper.find('.preview')
      expect(preview.html()).toContain(note.preview)
    })
  })

  describe('自定义笔记内容', () => {
    const content = '# Hello\n## World'
    const note = new Note(0)
    note.title = 'title'
    note.content = content
    note.favorite = true

    const wrapper = shallowMount(NoteEdit, {
      propsData: { note },
      localVue,
    })
    const el = wrapper.element

    test('输入框 [笔记标题]', () => {
      const input = el.querySelector('.toolbar input') as HTMLInputElement
      expect(input.value).toBe('title')
    })

    test('按钮', () => {
      const buttons = wrapper.findAll('button').wrappers
      expect(buttons.length).toBe(2)
      expect(['star', 'delete'].indexOf(buttons[0].text().trim())).not.toBe(-1)
      expect(['star', 'delete'].indexOf(buttons[1].text().trim())).not.toBe(-1)
    })

    test('MarkDown 输入区', () => {
      const area = wrapper.find('textarea')
      const el = area.element as HTMLTextAreaElement
      expect(el.value.trim()).toBe(content)
    })

    test('MarkDown 输出区', () => {
      const preview = wrapper.find('.preview')
      expect(preview.html()).toContain(note.preview)
    })

    test('创建时间', () => {
      expect(wrapper.find('.date .label').text()).toBe('创建时间')
      expect(wrapper.find('.date .value').text()).toBe(date(note.created))
    })

    test('行数', () => {
      expect(wrapper.find('.lines .label').text()).toBe('行数')
      expect(wrapper.find('.lines .value').text()).toBe('2')
    })

    test('字数', () => {
      expect(wrapper.find('.words .label').text()).toBe('字数')
      expect(wrapper.find('.words .value').text()).toBe('4')
    })

    test('收藏笔记', () => {
      const buttons = wrapper.findAll('button').wrappers
      const btn = buttons.find(value =>
        value.text().includes('star')
      ) as Wrapper<NoteEdit>

      btn.trigger('click')
      expect(wrapper.emitted().favorite).toBeTruthy()
      wrapper.vm.$emit('favorite')
      expect(wrapper.emitted().favorite.length).toBe(2)
    })

    test('删除笔记', () => {
      const buttons = wrapper.findAll('button').wrappers
      const btn = buttons.find(value =>
        value.text().includes('delete')
      ) as Wrapper<NoteEdit>

      btn.trigger('click')
      expect(wrapper.emitted().remove).toBeTruthy()
      wrapper.vm.$emit('remove')
      expect(wrapper.emitted().remove.length).toBe(2)
    })
  })
})
