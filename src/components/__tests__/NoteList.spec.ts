import { shallowMount, Wrapper } from '@vue/test-utils'
import NoteList from '../NoteList.vue'
import Note from '@/Note'
import Notes from '@/Notes'

describe('NoteList.vue', () => {
  describe('笔记列表为空', () => {
    // 挂载组件，创建包裹器
    // shallowMount 方法只挂载一个组件而不渲染其子组件 (即保留它们的存根)
    const wrapper = shallowMount(NoteList, {
      propsData: { notes: [], selectedNote: null },
    })
    // “添加笔记”按钮
    const addButton = wrapper.find('.toolbar button')

    test('初始化 props', () => {
      expect(Array.isArray(wrapper.props().notes)).toBe(true)
      expect(wrapper.props().notes.length).toBe(0)
      expect(wrapper.props().selectedNote).toBeNull()
    })

    test('添加笔记按钮', () => {
      expect(wrapper.contains('button')).toBe(true)
      expect(wrapper.findAll('button').length).toBe(1) // 只有一个 button 按钮
      expect(wrapper.html()).toContain('<i class="material-icons">add</i>')
      expect(wrapper.html()).toContain('添加笔记')
    })

    test('笔记列表为空', () => {
      expect(wrapper.contains('.notes')).toBe(true)
      const notes = wrapper.find('div.notes').element // 找到列表父元素
      expect(notes.querySelectorAll('*').length).toBe(0) // 无子元素
    })

    test('点击按钮时发送 add 事件', () => {
      addButton.trigger('click')
      expect(wrapper.emitted().add).toBeTruthy()
      wrapper.vm.$emit('add')
      expect(wrapper.emitted().add.length).toBe(2)
    })
  })

  describe('笔记列表包含笔记', () => {
    let wrapper: Wrapper<NoteList>
    let addButton: Wrapper<NoteList>

    let noteArr: Note[] = []
    let notes: Notes

    function delay() {
      return new Promise((resolve, reject) => {
        setTimeout(() => resolve(), 100)
      })
    }

    test('初始化', async () => {
      noteArr.push(new Note(noteArr.length))
      await delay()
      noteArr.push(new Note(noteArr.length))
      await delay()
      noteArr.push(new Note(noteArr.length))
      await delay()
      noteArr.push(new Note(noteArr.length))
      await delay()
      noteArr.push(new Note(noteArr.length))
      await delay()

      noteArr[3].favorite = true
      noteArr[4].favorite = true
      notes = new Notes(noteArr)

      wrapper = shallowMount(NoteList, {
        propsData: { notes: notes, selectedNote: null },
      })

      addButton = wrapper.find('.toolbar button') as Wrapper<NoteList>
    })

    test('添加笔记按钮', () => {
      expect(addButton.attributes('title')).toBe('已有 5 篇笔记')
    })

    test('笔记列表', () => {
      const notes = wrapper.find('div.notes').findAll('.note').wrappers
      expect(notes.length).toBe(5)

      expect(notes[0].text()).toContain('star')
      expect(notes[1].text()).toContain('star')
      expect(notes[0].text()).toContain('New note 4')
      expect(notes[1].text()).toContain('New note 5')
      expect(notes[2].text()).toBe('New note 1')
      expect(notes[3].text()).toBe('New note 2')
      expect(notes[4].text()).toBe('New note 3')
    })

    test('点击按钮时发送 select 事件', () => {
      const notes = wrapper.find('div.notes').findAll('.note').wrappers
      const note = notes[0]
      note.trigger('click')
      expect(wrapper.emitted().select).toBeTruthy()
      wrapper.vm.$emit('select', note)
      expect(wrapper.emitted().select.length).toBe(2)

      expect(wrapper.emitted().select[0][0].title).toBe('New note 4')
    })
  })
})
