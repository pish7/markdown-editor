import { VueConstructor } from 'vue'
import moment from 'moment'

export function date(time: number) {
  return moment(time).format('DD/MM/YY HH:mm')
}

// date 过滤器用于将时间戳转换为时间字符串
export default {
  install(Vue: VueConstructor, _options: any) {
    Vue.filter('date', date)
  },
}
