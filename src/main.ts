import Vue from 'vue'
import App from './App.vue'
import date from '@/filters/date'

Vue.config.productionTip = false

// 注册 date 过滤器，将时间戳转换为字符串
Vue.use(date)

new Vue({
  render: h => h(App),
}).$mount('#app')
