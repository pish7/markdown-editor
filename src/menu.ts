import { BrowserWindow, MenuItemConstructorOptions, Menu } from 'electron'
import * as mainProcess from './background'

/**
 * 创建应用程序菜单栏
 */
function createApplicationMenu() {
  /** 当前窗口 */
  const focusedWindow = BrowserWindow.getFocusedWindow()
  /** 当前窗口是否一个文件 */
  const hasFilePath = !!(
    (focusedWindow && focusedWindow.getRepresentedFilename()) // 窗口当前文件路径[macOS]
  )
  /** 菜单模板 */
  const template: MenuItemConstructorOptions[] = [
    {
      label: '文件',
      submenu: [
        {
          label: '新建文件',
          accelerator: 'CommandOrControl+N',
          click() {
            mainProcess.createWindow()
          },
        },
        {
          label: '打开文件',
          accelerator: 'CommandOrControl+O',
          click(item, focusedWindow) {
            mainProcess.getFileFromUser(focusedWindow)
          },
        },
        {
          label: '保存',
          accelerator: 'CommandOrControl+S',
          click(item, focusedWindow) {
            focusedWindow.webContents.send('save-markdown')
          },
        },
        {
          label: '导出 HTML',
          accelerator: 'Shift+CommandOrControl+S',
          click(item, focusedWindow) {
            focusedWindow.webContents.send('save-html')
          },
        },
        { type: 'separator' },
        {
          label: '在资源管理器中显示',
          enabled: hasFilePath,
          click(item, focusedWindow) {
            focusedWindow.webContents.send('show-file')
          },
        },
        {
          label: '使用默认应用打开',
          enabled: hasFilePath,
          click(item, focusedWindow) {
            focusedWindow.webContents.send('open-in-default')
          },
        },
      ],
    },
    {
      label: '编辑',
      submenu: [
        {
          label: '撤消',
          accelerator: 'CommandOrControl+Z',
          // 通过 role 属性快速实现操作系统功能
          role: 'undo',
        },
        {
          label: '恢复',
          accelerator: 'Shift+CommandOrControl+Z',
          role: 'redo',
        },
        { type: 'separator' },
        {
          label: '剪切',
          accelerator: 'CommandOrControl+X',
          role: 'cut',
        },
        {
          label: '复制',
          accelerator: 'CommandOrControl+C',
          role: 'copy',
        },
        {
          label: '粘贴',
          accelerator: 'CommandOrControl+V',
          role: 'paste',
        },
        {
          label: '全选',
          accelerator: 'CommandOrControl+A',
          role: 'selectall',
        },
      ],
    },
    {
      label: '窗口',
      submenu: [
        {
          label: '最小化',
          accelerator: 'CommandOrControl+M',
          role: 'minimize',
        },
        {
          label: '关闭',
          accelerator: 'CmdOrCtrl+W',
          role: 'close',
        },
      ],
    },
    {
      label: '开发',
      role: 'help',
      submenu: [
        {
          label: '重新加载窗口',
          accelerator: 'CmdOrCtrl+R',
          click() {
            focusedWindow && focusedWindow.reload()
          },
        },
        {
          label: '开发者工具',
          accelerator: 'F12',
          click(item, focusedWindow) {
            if (focusedWindow) {
              focusedWindow.webContents.toggleDevTools()
            }
          },
        },
      ],
    },
  ]

  Menu.setApplicationMenu(Menu.buildFromTemplate(template))
}

export default createApplicationMenu
