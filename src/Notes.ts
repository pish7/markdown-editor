import Note from './Note'

/**
 * 记录一组笔记
 * @class
 */
export default class Notes {
  private _notes: Note[] = []

  /**
   * 用可迭代对象来创建笔记组
   * @param {Iterable<Object|number>=} notes 可迭代对象，用于初始化每个笔记
   */
  constructor(notes?: Iterable<any>) {
    if (notes) {
      for (let note of notes) {
        this._notes.push(new Note(note))
      }
    }
  }

  private toJSON() {
    return this._notes
  }

  /**
   * 获取笔记数量
   * @return {number} 笔记的数量
   */
  public get length(): number {
    return this._notes.length
  }

  /**
   * 获取排序后的笔记列表
   * @return {Note[]} 排序后的笔记列表
   */
  public get notes(): Note[] {
    // 由于sort方法会直接修改源数组，这里使用slice方法创建新的副本。
    return this._notes
      .slice()
      .sort((a, b) => a.created - b.created)
      .sort((a, b) => (a.favorite === b.favorite ? 0 : a.favorite ? -1 : 1))
  }

  /**
   * 是否有笔记内容发生了改动
   * @return {boolean}
   */
  public get changed(): boolean {
    return this._notes.some(note => note.textChanged)
  }

  /**
   * 根据笔记的ID检索笔记
   * @param id 笔记的ID
   * @return {Note|undefined} 检索到的笔记
   */
  public getNoteFromId(id: string): Note | undefined {
    return this._notes.find((note: Note) => note.id == id)
  }

  /**
   * 添加一个笔记
   * @param note 添加的笔记
   */
  public add(note: Note) {
    this._notes.push(note)
  }

  /**
   * 删除一个指定的笔记
   * @param note 要删除的笔记
   */
  public delete(note: Note) {
    const index = this._notes.indexOf(note)
    if (index !== -1) {
      this._notes.splice(index, 1)
    }
  }
}
