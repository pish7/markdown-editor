module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  verbose: true,
  testMatch: ['<rootDir>/tests/e2e/**/*.spec.(js|jsx|ts|tsx)'],
}
