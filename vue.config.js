module.exports = {
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        appId: 'io.gitee.pish7.electron-markdown-vue-ts-sass',
        productName: 'electron-markdown-vue-ts-sass',
        win: {
          target: 'portable',
          // target: ['portable', 'nsis', 'msi'],
          icon: 'public/favicon.ico',
          extraFiles: [
            './public/**/*',
            // {
            //   from: './src/assets',
            //   to: 'assets',
            //   filter: ['**/*'],
            // },
          ],
        },
        nsis: {
          oneClick: false,
          allowToChangeInstallationDirectory: true,
          installerHeaderIcon: 'public/favicon.ico',
        },
      },
    },
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "~@/style/_variable.scss";`,
      },
    },
  },
}
